FROM node:12.16.3-alpine3.9


# Expose the default port
#EXPOSE 4000

# Create/Set the working directory
WORKDIR /server

RUN npm install -g nodemon

#CMD [“node”, server.js”]

