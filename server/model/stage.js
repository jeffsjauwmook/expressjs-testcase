const { Schema, model } = require('mongoose');


const stageSchema = new Schema({
    name: String,
    description: String,
});

module.exports = model('Stage', stageSchema);