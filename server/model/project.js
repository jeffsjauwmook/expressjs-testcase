const { Schema, model } = require('mongoose');


const projectSchema = new Schema({
    title: String,
    description: String,
    userId: Schema.Types.ObjectId,
    stageId: Schema.Types.ObjectId,

})
module.exports = model('Project', projectSchema);