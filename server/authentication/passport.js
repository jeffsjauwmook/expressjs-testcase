const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const User = require('../model/user');
const passportJWT = require("passport-jwt");
const JWTStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;
const bcrypt = require('bcrypt-nodejs');


passport.use(new LocalStrategy(
    function (username, password, done) {
        User.findOne({ username: username }).then(user => {

            if (!user) {
                return cb(null, false, { message: 'Incorrect username.' });
            }
            if (!bcrypt.compareSync(password, user.password)) {
                return done(null, false, { message: 'Incorrect password.' });
            }
            return done(null, user);

        })
            .catch(err => {
                console.log(err);
            });;
    }
));


passport.use(new JWTStrategy({
    jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
    secretOrKey: 'secret'
},
    function (jwtPayload, cb) {

        return User.findOne({ id: jwtPayload.id })
            .then(user => {
                return cb(null, User);
            })
            .catch(err => {
                return cb(err);
            });
    }
));

module.exports = passport;