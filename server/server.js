const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const indexRouter = require('./routes/index');
const auth = require('./routes/auth');
const mongoose = require('./database/mongoose');
require('./authentication/passport')
const cors = require('cors');
const port = process.env.PORT || 4000;

const app = express();

const devEnv = app.get('env') == 'development';
app.use(morgan(devEnv ? 'dev' : 'combined'));

if (app.get('env') === 'production') {
     app.set('trust proxy', 1) // trust first proxy
     sess.cookie.secure = true // serve secure cookies
}
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

app.use('/auth', auth);
app.use('/', indexRouter)


app.listen(port, () => {
     console.log('Listening on port 4000');
})

