const express = require('express');
const router = express.Router();
const graphglHTTP = require('express-graphql');
const schema = require('../schema/schema');
const secureSchema = require('../schema/secureSchema');
const passport = require('../authentication/passport');

router.use('/graphql', graphglHTTP({
    graphiql: true,
    schema: schema

}));

router.post('/login', passport.authenticate('local', {
    successRedirect: '/graphql',
    failureFlash: 'no!',
    failWithError: true
}));

router.use('/secure/graphql', passport.authenticate('jwt', { session: false }), graphglHTTP({
    graphiql: true,
    schema: secureSchema

}));


module.exports = router;