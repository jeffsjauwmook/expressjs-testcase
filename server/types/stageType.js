const graphql = require("graphql");

const {
    GraphQLObjectType,
    GraphQLID,
    GraphQLString,
} = graphql;

const StageType = new GraphQLObjectType({
    name: "Stage",
    description: "Stage that a project is in",
    fields: () => ({
        id: { type: GraphQLID },
        name: { type: GraphQLString },
        description: { type: GraphQLString }
    }),
});

module.exports = StageType;