const graphql = require("graphql");
const Project = require("../model/project");
const ProjectType = require('../types/projectType')


const {
    GraphQLObjectType,
    GraphQLID,
    GraphQLString,
} = graphql;

const UserType = new GraphQLObjectType({
    name: "User",
    description: "Project for user...",
    fields: () => ({
        id: { type: GraphQLID },
        username: { type: GraphQLString }
    }),
});

module.exports = UserType;