const graphql = require("graphql");
const User = require("../model/user");
const Stage = require("../model/stage");
const UserType = require('../types/userType')
const StageType = require('../types/stageType')

const {
    GraphQLObjectType,
    GraphQLID,
    GraphQLString,
    GraphQLList,
} = graphql;

const ProjectType = new GraphQLObjectType({
    name: "Project",
    description: "Project description",
    user: "owner of the project",
    stage: "stage of the project",
    fields: () => ({
        id: { type: GraphQLID },
        title: { type: GraphQLString },
        description: { type: GraphQLString },
        userId: { type: GraphQLID },
        stageId: { type: GraphQLID },
        /** resolvers werkt niet iets met de type kwam er niet uit 
        user: {
            type: UserType,
            resolve(parent, args) {
                return User.findById(parent.user);
            },
        },
        stage: {
            type: StageType,
            resolve(parent, args) {
                return Stage.findById(parent.stage);
            },
        }, **/
    }),
});

module.exports = ProjectType;