const graphql = require("graphql");
var _ = require("lodash");
const User = require("../model/user");
const Project = require("../model/project");
const Stage = require("../model/stage");
const UserType = require('../types/userType')
const ProjectType = require('../types/projectType')
const StageType = require('../types/stageType')

const {
    GraphQLObjectType,
    GraphQLID,
    GraphQLString,
    GraphQLNonNull,
} = graphql;

const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: () => ({
        CreateUser: {
            type: UserType,
            args: {
                username: { type: new GraphQLNonNull(GraphQLString) },
                password: { type: new GraphQLNonNull(GraphQLString) }
            },

            resolve(parent, args) {
                let user = new User({
                    username: args.username,
                    password: bcrypt.hashSync(args.password),
                });
                return user.save();
            }
        },

        UpdateUser: {
            type: UserType,
            args: {
                id: { type: new GraphQLNonNull(GraphQLString) },
                username: { type: new GraphQLNonNull(GraphQLString) },
                password: { type: new GraphQLNonNull(GraphQLString) }
            },
            resolve(parent, args) {
                return updatedUser = User.findByIdAndUpdate(
                    args.id,
                    {
                        $set: {
                            username: args.username,
                            password: args.password
                        }
                    },
                    { new: true }
                )

            }
        },
        RemoveUser: {
            type: UserType,
            args: {
                id: { type: new GraphQLNonNull(GraphQLString) }
            },
            resolve(parent, args) {
                let removedUser = User.findByIdAndRemove(
                    args.id
                ).exec();

                if (!removedUser) {
                    throw new ("Error");
                }

                return removedUser;
            }
        },
        CreateStage: {
            type: StageType,
            args: {

                name: { type: new GraphQLNonNull(GraphQLString) },
                description: { type: GraphQLString },

            },
            resolve(parent, args) {
                let stage = new Stage({
                    name: args.name,
                    description: args.description,
                });
                return stage.save();
            }
        },
        UpdateStage: {
            type: StageType,
            args: {
                id: { type: new GraphQLNonNull(GraphQLString) },
                name: { type: new GraphQLNonNull(GraphQLString) },
            },
            resolve(parent, args) {
                return updatedStage = Stage.findByIdAndUpdate(
                    args.id,
                    {
                        $set: {
                            name: args.name,
                        }
                    },
                    { new: true }
                )
            }
        },
        RemoveStage: {
            type: StageType,
            args: {
                id: { type: new GraphQLNonNull(GraphQLString) }
            },
            resolve(parent, args) {
                let removedStage = Stage.findByIdAndRemove(
                    args.id
                ).exec();
                if (!removedStage) {
                    throw new ("Error");
                }
                return removedStage;
            }
        },
        CreateProject: {
            type: ProjectType,
            args: {
                title: { type: new GraphQLNonNull(GraphQLString) },
                description: { type: new GraphQLNonNull(GraphQLString) },
                userId: { type: new GraphQLNonNull(GraphQLID) },
                stageId: { type: new GraphQLNonNull(GraphQLID) },
            },
            resolve(parent, args) {
                let project = new Project({
                    title: args.title,
                    description: args.description,
                    userId: args.userId,
                    stageId: args.stageId
                });
                return project.save();
            }
        },
        UpdateProject: {
            type: ProjectType,
            args: {
                title: { type: new GraphQLNonNull(GraphQLString) },
                description: { type: new GraphQLNonNull(GraphQLString) },
                userId: { type: new GraphQLNonNull(GraphQLID) },
                stageId: { type: new GraphQLNonNull(GraphQLID) },
            },
            resolve(parent, args) {
                return updatedProject = Project.findByIdAndUpdate(
                    args.id,
                    {
                        $set: {
                            title: args.title,
                            description: args.description,
                            userId: args.userId,
                            stageId: args.stageId
                        }
                    },
                    { new: true })
            }
        },
        RemoveProject: {
            type: ProjectType,
            args: {
                id: { type: new GraphQLNonNull(GraphQLString) }
            },
            resolve(parent, args) {
                let removedProject = Project.findByIdAndRemove(
                    args.id
                ).exec();
                if (!removedProject) {
                    throw new ("Error");
                }
                return removedProject;
            }
        },
    }),

});

module.exports = Mutation;