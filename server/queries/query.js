const graphql = require("graphql");
var _ = require("lodash");
const User = require("../model/user");
const Project = require("../model/project");
const Stage = require("../model/stage");
const UserType = require('../types/userType')
const ProjectType = require('../types/projectType')
const StageType = require('../types/stageType')

const {
    GraphQLObjectType,
    GraphQLID,
    GraphQLString,
    GraphQLList,
} = graphql;

const RootQuery = new GraphQLObjectType({
    name: "RootQueryType",
    description: "Description",
    fields: {
        user: {
            type: UserType,
            args: { id: { type: GraphQLString } },
            resolve(parent, args) {
                return User.findById(args.id);
            },
        },
        users: {
            type: new GraphQLList(UserType),
            resolve(parent, args) {
                return User.find({});
            },
        },
        project: {
            type: ProjectType,
            args: { id: { type: GraphQLID } },
            resolve(parent, args) {
                return Project.findById(args.id);
            },
        },
        projects: {
            type: new GraphQLList(ProjectType),
            resolve() {
                return Project.find({});
            },
        },
        stage: {
            type: StageType,
            args: { id: { type: GraphQLID } },
            resolve(parent, args) {
                return Stage.findById(args.id);
            },
        },
        stages: {
            type: new GraphQLList(StageType),
            args: { id: { type: GraphQLID } },
            resolve() {
                return Stage.find({});
            }
        },
    },
});

module.exports = RootQuery;