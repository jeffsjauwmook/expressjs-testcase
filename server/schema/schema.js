const graphql = require("graphql");
const RootQuery = require('../queries/query')
const { GraphQLSchema } = graphql;

module.exports = new GraphQLSchema({
    query: RootQuery,
});
