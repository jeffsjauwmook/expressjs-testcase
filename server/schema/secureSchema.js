const graphql = require("graphql");
const RootQuery = require('../queries/query')
const Mutation = require('../queries/mutation')
const { GraphQLSchema } = graphql;

module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation: Mutation
})





