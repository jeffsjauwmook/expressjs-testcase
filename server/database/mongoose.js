const mongoose = require('mongoose');

mongoose.connect('mongodb://172.24.238.12:27017/backend',
    { useNewUrlParser: true, useUnifiedTopology: true }, (err, connection) => {
        if (err) {
            console.log('Error connecting to db');
            console.log(err);
        } else {
            console.log('connected to db ');
            // console.log(connection);
        }
    });
module.exports = mongoose;